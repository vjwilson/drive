import React, { Component } from 'react';
import Rodal from 'rodal';

// Include styles
import 'rodal/lib/rodal.css';
import './App.css';

// Include Firebase (database)
import firebase from './firebase.js';

class App extends Component {
  constructor() {
    super();

    this.state = {
      description: '',
      mood: '',
      name: '',
      email: '',
      comments: [],
      modalVisible: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleOptionChange = this.handleOptionChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleOptionChange(event) {
    this.setState({
      mood: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const commentsRef = firebase.database().ref('comments');

    const comment = {
      description: this.state.description,
      mood: this.state.mood,
      name: this.state.name,
      email: this.state.email,
    }

    commentsRef.push(comment);

    this.hideModal();

    this.setState({
      description: '',
      mood: '',
      name: '',
      email: ''
    })
  }

  showModal() {
    this.setState({ modalVisible: true });
  }

  hideModal() {
    this.setState({ modalVisible: false });
  }

  componentDidMount() {
    const commentsRef = firebase.database().ref('comments');

    console.log("Initial Ref", commentsRef);

    commentsRef.on('value', (snapshot) => {
      let comments = snapshot.val();
      let newState = [];

      for (let comment in comments) {
        console.log("Comments", comment);
        newState.push({
          id: comment,
          description: comments[comment].description,
          mood: comments[comment].mood,
          name: comments[comment].name,
          email: comments[comment].email
        })
      }

      this.setState({
        comments: newState
      });

      console.log("New State", newState);
      console.log("State", this.state.comments);
    });

    console.log("State", this.state.comments);
  }

  removeComment(commentId) {
    const comments = firebase.database().ref(`/comments/${commentId}`);
    comments.remove();
  }


  render() {
    console.log("Render", this.state.comments);

    return (
      <div className="app">
        <header className="masthead" role="banner">
          <svg className="logo"><use xlinkHref="#n7nu-logo" /></svg>

          <div className="intro">
            <p lang="ar-SA">حَـنـسوق في ٢٠١٨! شاركـونا أحاسيـسكم، ملاحظاتكم، وآراءكم.هاذي مساحتكم.</p>
            <p lang="en-US">Are going to drive in 2018! Share your feelings and opinions.</p>
          </div>
        </header>

        <main className="content" role="main">
          <button className="primary-button" onClick={this.showModal}><span lang="ar-SA">اسـمع صـوتي!</span> <span lang="en-US">Hear me roar!</span></button>       
          
          { this.state.comments !== undefined ?
            <ul className="comments">
              <li>There are comments!</li>
              {this.state.comments.map((comment) => {
                <li className="comment" data-mood={comment.mood} key={comment.id} >
                  <p className="comment-description">{comment.description}</p>
                  <button className="primary-button small-button" onClick={() => this.removeComment(comment.id)}>Delete</button>
                </li>
              })}
            </ul>
            : null
          }
        </main>

        <footer id="footer" role="contentinfo">

        </footer>

        <Rodal visible={this.state.modalVisible} onClose={this.hideModal} height="485">
          <form className="roar-form" onSubmit={this.handleSubmit}>
            <div className="input-group">
              <label htmlFor="description">
                <span lang="ar-SA">(دولور سيت (150 حرف</span>
                <span lang="en-US">Description (150 ch)</span>
              </label>
              <textarea id="description" name="description" onChange={this.handleChange} value={this.state.description} />
            </div>

            <div className="input-group mood-options">
              <label className="mood-option-happy">
                <span lang="ar-SA">?</span>
                <span lang="en-US">Happy</span>
                <input className="mood-option-selection" name="mood-option-selection" type="radio" value="happy" onChange={this.handleOptionChange}/>
                <svg className="mood-option-icon"><use xlinkHref="#icon-happy" /></svg>
              </label>
              <label className="mood-option-meh">
                <span lang="ar-SA">?</span>
                <span lang="en-US">Meh</span>
                <input className="mood-option-selection" name="mood-option-selection" type="radio" value="meh" onChange={this.handleOptionChange} />
                <svg className="mood-option-icon"><use xlinkHref="#icon-meh" /></svg>
              </label>
              <label className="mood-option-sad">
                <span lang="ar-SA">?</span>
                <span lang="en-US">Sad</span>
                <input className="mood-option-selection" name="mood-option-selection" type="radio" value="sad" onChange={this.handleOptionChange} />
                <svg className="mood-option-icon"><use xlinkHref="#icon-sad" /></svg>
              </label>
            </div>

            <div className="input-group">
              <label htmlFor="email">
                <span lang="ar-SA">(غيز لازم)</span>
                <span lang="en-US">Email (optional)</span>
              </label>
              <input id="email" name="email" type="email" onChange={this.handleChange} value={this.state.email}/>
            </div>

            <div className="input-group">
              <label htmlFor="name">
                <span lang="ar-SA">(الاسم / اللقب   (غيز لازم</span>
                <span lang="en-US">Name / nickname (optional)</span>
              </label>
              <input id="name" name="name" type="text" onChange={this.handleChange} value={this.state.name} />
            </div>

            <button className="secondary-button" type="submit">
              <span lang="ar-SA">إسـتعمـل</span>
              <span lang="en-US">Share</span>
            </button>
          </form>
        </Rodal>
      </div>
    );
  }
}

export default App;
